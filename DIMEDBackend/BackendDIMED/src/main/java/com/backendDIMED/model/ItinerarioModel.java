package com.backendDIMED.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "itinerario")
public class ItinerarioModel implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String idlinha;
	private String nome;
	private String codigo;
	
	@JsonManagedReference
	@OneToMany( mappedBy = "itinerario", cascade = CascadeType.ALL, fetch= FetchType.EAGER)
	private List<LatLonModel> lagLon = new ArrayList<>();
	
	public ItinerarioModel() {
		super();
	}

	public List<LatLonModel> getLagLon() {
		return lagLon;
	}

	public void setLagLon(List<LatLonModel> lagLon) {
		this.lagLon = lagLon;
	}

	public String getIdlinha() {
		return idlinha;
	}

	public void setIdlinha(String idlinha) {
		this.idlinha = idlinha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public ItinerarioModel(String idlinha, String nome, String codigo, List<LatLonModel> lagLon) {
		super();
		this.idlinha = idlinha;
		this.nome = nome;
		this.codigo = codigo;
		this.lagLon = lagLon;
	}

}
