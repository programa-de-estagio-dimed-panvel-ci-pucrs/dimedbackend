package com.backendDIMED.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PontoTaxiModel implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPontoTaxi;
	private String nomePonto;
	private double latitude;
	private double longitude;
	private Date dataHoraCadastro;
	
	
	public int getIdPontoTaxi() {
		return idPontoTaxi;
	}
	public void setIdPontoTaxi(int idPontoTaxi) {
		this.idPontoTaxi = idPontoTaxi;
	}
	public String getNomePonto() {
		return nomePonto;
	}
	public void setNomePonto(String nomePonto) {
		this.nomePonto = nomePonto;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public Date getDataHoraCadastro() {
		return dataHoraCadastro;
	}
	public void setDataHoraCadastro(Date dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}
	
	
	
	
	
}
