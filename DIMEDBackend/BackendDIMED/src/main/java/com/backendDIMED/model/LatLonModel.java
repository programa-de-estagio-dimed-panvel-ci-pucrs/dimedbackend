package com.backendDIMED.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "latLon")
public class LatLonModel {
	
	    @Id
	    private String id;
		private double lat;
		private double lng;
		
		public LatLonModel() {
			super();
		}
		
		@JsonBackReference
		@ManyToOne
		@JoinColumn(name = "itinerario")
		private ItinerarioModel itinerario;
		
		
		public ItinerarioModel getItinerario() {
			return itinerario;
		}
		public void setItinerario(ItinerarioModel itinerario) {
			this.itinerario = itinerario;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public double getLat() {
			return lat;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		public double getLng() {
			return lng;
		}
		public void setLng(double lng) {
			this.lng = lng;
		}
		public LatLonModel(String id, double lat, double lng) {
			super();
			this.id = id;
			this.lat = lat;
			this.lng = lng;
		}
	
}
