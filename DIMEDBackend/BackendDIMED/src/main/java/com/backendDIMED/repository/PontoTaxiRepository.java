package com.backendDIMED.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.backendDIMED.model.PontoTaxiModel;

@Repository
public interface PontoTaxiRepository extends JpaRepository<PontoTaxiModel, Integer> {

}
