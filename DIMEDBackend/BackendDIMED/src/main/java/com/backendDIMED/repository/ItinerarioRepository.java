package com.backendDIMED.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.backendDIMED.model.ItinerarioModel;

@Repository
public interface ItinerarioRepository extends JpaRepository<ItinerarioModel, String> {

}
