package com.backendDIMED.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.backendDIMED.model.LinhaModel;

@Repository
public interface LinhaRepository extends JpaRepository<LinhaModel, Integer>  {
	
	
	
}
