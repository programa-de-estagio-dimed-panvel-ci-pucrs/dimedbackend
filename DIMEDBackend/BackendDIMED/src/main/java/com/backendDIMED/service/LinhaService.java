package com.backendDIMED.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.backendDIMED.model.LinhaModel;

@Service
public interface LinhaService {
	Collection <LinhaModel> findLine();

	Collection <LinhaModel> findLineByNome(String nome);

	LinhaModel saveLine(int id, LinhaModel linha);

	Collection<LinhaModel> findAll();

	LinhaModel findLineById(int id);

	LinhaModel updateLine(int id, LinhaModel linha);

	LinhaModel deleteLine(int id);
	
	Collection<LinhaModel> findLineByLightning(double lat, double lng, double raio) throws InterruptedException;

}
