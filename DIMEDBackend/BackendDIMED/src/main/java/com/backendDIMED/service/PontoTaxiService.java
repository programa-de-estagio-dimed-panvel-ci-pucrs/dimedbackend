package com.backendDIMED.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

import org.springframework.stereotype.Service;

import com.backendDIMED.model.PontoTaxiModel;

@Service
public interface PontoTaxiService {

	Collection <PontoTaxiModel> findTaxiPoint() throws IOException, ParseException;
	
	PontoTaxiModel saveTaxiPoint(String caminho, String pontoTaxiNovo) throws IOException, ParseException;
	
	Collection <PontoTaxiModel> findAll();
}
