package com.backendDIMED.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backendDIMED.model.ItinerarioModel;
import com.backendDIMED.service.ItinerarioService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Itinerário", tags = { "Itinerário API" })
@RestController
@RequestMapping("api/itinerario")
public class ItinerarioController {

	@Autowired
	private ItinerarioService itinerarioService;

	@ApiOperation(value = "Esse método lista itinerário filtrando pelo id.", notes = "Esse método está consumindo a API externa, ao trazer o resultado dessa consulta o método persiste ela no banco de dados h2. ")
	@GetMapping("/{idlinha}")
	public ResponseEntity<ItinerarioModel> findItinerary(@PathVariable("idlinha") int idlinha)
			throws JsonMappingException, JsonProcessingException {
		ItinerarioModel itinerarioModel = new ItinerarioModel();
		itinerarioModel = itinerarioService.findItinerary(idlinha);
		if (itinerarioModel == null) {
			return new ResponseEntity<ItinerarioModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItinerarioModel>(itinerarioModel, HttpStatus.OK);
	}

	@ApiOperation(value = "Esse método lista todos os itinerário", notes = "Esse método está consumindo o banco de dados h2.")
	@GetMapping("/busca")
	public ResponseEntity<Collection<ItinerarioModel>> findAll() {
		Collection<ItinerarioModel> result = new ArrayList<>();
		result = itinerarioService.findAll();
		if (result == null) {
			return new ResponseEntity<Collection<ItinerarioModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<ItinerarioModel>>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "Esse método lista o itinerário filtrando pelo id, caso o id passado não exista, o método retorna status HTTP NOT_FOUND.", notes=" Esse método está consumindo o banco de dados H2.")
	@GetMapping("/busca/{id}")
	public ResponseEntity<ItinerarioModel> findById(@PathVariable("id") String id) {
		ItinerarioModel itinerarioModel = new ItinerarioModel();
		itinerarioModel = itinerarioService.findById(id);
		if (itinerarioModel == null) {
			return new ResponseEntity<ItinerarioModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItinerarioModel>(itinerarioModel, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Esse método exclui uma itinerário filtrando pelo o id, caso o id passado não exista, o método retorna status HTTP NOT_FOUND.", notes=" Esse método está consumindo o banco de dados H2.")
	@DeleteMapping("/exclui/{id}")
	public ResponseEntity<ItinerarioModel> deleteItinerary(@PathVariable("id") String id) {
		ItinerarioModel itinerarioModel = new ItinerarioModel();
		itinerarioModel = itinerarioService.deleteItinerary(id);
		if (itinerarioModel == null) {
			return new ResponseEntity<ItinerarioModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItinerarioModel>(itinerarioModel, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Esse método atualiza um itinerário existente, caso o itinerário passado não exista, o metodo retorna status HTTP NOT_FOUND.", notes=" Esse método está consumindo o banco de dados H2.")
	@PutMapping("/atualiza/{id}")
	public ResponseEntity<ItinerarioModel> updateItinerary(@PathVariable("id") String id,  @RequestBody ItinerarioModel itinerarioModelAtualiza ) {
		ItinerarioModel itinerarioModel = new ItinerarioModel();
		itinerarioModel = itinerarioService.updateItinerary(id, itinerarioModelAtualiza);
		if (itinerarioModel == null) {
			return new ResponseEntity<ItinerarioModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItinerarioModel>(itinerarioModel, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Esse método adiciona um novo itinerário.", notes=" Esse método está consumindo o banco de dados H2.")
	@PostMapping("/adiciona/{id}")
	public ResponseEntity<ItinerarioModel> saveItinerary(@PathVariable("id") String id,  @RequestBody ItinerarioModel itinerarioModelAdiciona ) {
		ItinerarioModel itinerarioModel = new ItinerarioModel();
		itinerarioModel = itinerarioService.saveItinerary(id, itinerarioModelAdiciona);
		if (itinerarioModel == null) {
			return new ResponseEntity<ItinerarioModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItinerarioModel>(itinerarioModel, HttpStatus.OK);
	}

}
