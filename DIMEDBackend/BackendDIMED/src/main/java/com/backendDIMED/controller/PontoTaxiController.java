package com.backendDIMED.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backendDIMED.model.PontoTaxiModel;
import com.backendDIMED.service.PontoTaxiService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Ponto de Táxi", tags = { "Ponto de Táxi API" })
@RestController
@RequestMapping("api/ponto")
public class PontoTaxiController {

	@Autowired
	PontoTaxiService pontoTaxiService;
	
	static String path = "PontoTaxi.txt";
	
	@ApiOperation(value = "Esse método lista todos os pontos de táxi presente no arquivo PontoTaxi.txt.", notes=" Esse método está consumindo o arquivo PontoTaxi.txt.")
	@GetMapping("/busca")
	public ResponseEntity<Collection<PontoTaxiModel>> findTaxiPoint() throws IOException, ParseException {
		Collection<PontoTaxiModel> result = new ArrayList<>();
		result = pontoTaxiService.findTaxiPoint();
		if (result == null) {
			return new ResponseEntity<Collection<PontoTaxiModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<PontoTaxiModel>>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Esse método salva um novo ponto de táxi, tanto no arquivo como no banco de dados.")
	@PostMapping("/adiciona")
	public ResponseEntity<PontoTaxiModel> saveTaxiPoint(@RequestBody String pontoTaxiNovo) throws IOException, ParseException {
		PontoTaxiModel result = new PontoTaxiModel();
		result = pontoTaxiService.saveTaxiPoint(path, pontoTaxiNovo);
		if (result == null) {
			return new ResponseEntity<PontoTaxiModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<PontoTaxiModel>(result, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Esse método lista yodos os pontos de táxi.", notes = "Esse método está consumindo o banco de dados h2.")
	@GetMapping("/banco/busca")
	public ResponseEntity<Collection<PontoTaxiModel>> findAll() {
		Collection<PontoTaxiModel> result = new ArrayList<>();
		result = pontoTaxiService.findAll();
		if (result == null) {
			return new ResponseEntity<Collection<PontoTaxiModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<PontoTaxiModel>>(result, HttpStatus.OK);
	}
}
