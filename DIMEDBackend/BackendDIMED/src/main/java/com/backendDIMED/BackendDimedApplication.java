package com.backendDIMED;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendDimedApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendDimedApplication.class, args);
	}

}
