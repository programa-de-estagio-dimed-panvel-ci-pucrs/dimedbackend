package com.backendDIMED.util;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;



@Configuration
@EnableSwagger2
@ComponentScan(basePackages="com.backendDIMED.controller")
public class ApplicationSwaggerConfig {

	   @Bean
	   public Docket customDocket(){
	      return new Docket(DocumentationType.SWAGGER_2)
	    		  .select()
	    		  .apis(RequestHandlerSelectors.basePackage("com.backendDIMED"))
	              .paths(PathSelectors.any())
	              .build()
	              .apiInfo(getApiInfo());
	   }
	   
	   private ApiInfo getApiInfo() {
		   return new ApiInfo(
			"Spring backend Api Documentation",
			"This is REST API documentation of the Spring backend.",
			"1.0",
			"PoC backend terms of service",
			new Contact(
					"Camila Borba Rocha",
					"https://gitlab.com/programa-de-estagio-dimed-panvel-ci-pucrs/dimedbackend", null
					),
			"Apache 2.0",
			"http://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
	   }


}
