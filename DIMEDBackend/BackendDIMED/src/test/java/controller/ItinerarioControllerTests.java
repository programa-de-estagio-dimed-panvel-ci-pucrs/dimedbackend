package controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.backendDIMED.controller.ItinerarioController;
import com.backendDIMED.model.ItinerarioModel;
import com.backendDIMED.model.LatLonModel;
import com.backendDIMED.service.ItinerarioService;


import service.ApplicationTestConfig;

@SpringBootTest(classes = ItinerarioControllerTests.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationTestConfig.class)
@WebAppConfiguration
public class ItinerarioControllerTests {

	@InjectMocks
	private ItinerarioController itinerarioController;

	@MockBean
	private ItinerarioService itinerarioService;

	private MockMvc mockMvc;

	List<ItinerarioModel> itinerarios = new ArrayList<ItinerarioModel>();

	ItinerarioModel itinerarioModel1 = new ItinerarioModel();

	@Before
	public void initLinha() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(itinerarioController)
				.setControllerAdvice(new ExceptionControllerAdvice()).build();

		List<ItinerarioModel> itinerarioModel = new ArrayList<ItinerarioModel>();

		ItinerarioModel i1 = new ItinerarioModel();

		LatLonModel latLonModel = new LatLonModel();
		latLonModel.setId("0");
		latLonModel.setLat(-30.029855920942);
		latLonModel.setLng(-51.227864350442);
		latLonModel.setItinerario(i1);

		LatLonModel latLonModel1 = new LatLonModel();
		latLonModel1.setId("1");
		latLonModel1.setLat(-30.030426920942);
		latLonModel1.setLng(-51.227788350442);
		latLonModel1.setItinerario(i1);

		i1.setCodigo("02.1-2");
		i1.setIdlinha("1");
		i1.setNome("MENINO DEUS");
		i1.getLagLon().add(latLonModel);
		i1.getLagLon().add(latLonModel1);

		itinerarioModel.add(i1);

		itinerarios = itinerarioModel;

		itinerarioModel1 = i1;

	}

	@Test
	public void testFindItinerary() throws Exception {
		Mockito.when(itinerarioService.findItinerary(1)).thenReturn(itinerarioModel1);
		this.mockMvc.perform(get("/api/itinerario/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void testFindAll() throws Exception {
		Mockito.when(itinerarioService.findAll()).thenReturn(itinerarios);
		this.mockMvc.perform(get("/api/itinerario/busca").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testFindById() throws Exception {
		Mockito.when(itinerarioService.findById(itinerarioModel1.getIdlinha())).thenReturn(itinerarioModel1);
		this.mockMvc.perform(get("/api/itinerario/busca/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void deleteItinerary() throws Exception {
		Mockito.when(itinerarioService.deleteItinerary(itinerarioModel1.getIdlinha())).thenReturn(itinerarioModel1);
		this.mockMvc.perform(delete("/api/itinerario/exclui/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
}
