package com.backendDIMED.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backendDIMED.model.ItinerarioModel;
import com.backendDIMED.model.LinhaModel;

import com.backendDIMED.service.LinhaService;

@RestController
@RequestMapping("api/linha")
public class LinhaController {

	@Autowired
	private LinhaService linhaService;
	
	

	@GetMapping
	public ResponseEntity<List<LinhaModel>> findLine() {
		ResponseEntity<List<LinhaModel>> result = linhaService.findLine();
		return result;
	}

	// parte 2 
	@GetMapping("/nome/{nome}")
	public ResponseEntity<List<LinhaModel>> findLineByNome(@PathVariable("nome") String nome) {
		ResponseEntity<List<LinhaModel>> result = linhaService.findLineByNome(nome);
		return result;
	}
	
	// parte 3 
	@PostMapping("/adiciona/{id}")
	public ResponseEntity<LinhaModel> saveLine(@PathVariable("id") int id, @RequestBody LinhaModel linhaDeOnibusModelAdiciona) {
		LinhaModel linhaModel = new LinhaModel ();
		linhaModel = linhaService.saveLine(id, linhaDeOnibusModelAdiciona);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}
	
	@PutMapping("/atualiza/{id}")
	public ResponseEntity<LinhaModel> updateLine(@PathVariable("id") int id, @RequestBody LinhaModel linhaDeOnibusModel) {
		LinhaModel linhaModel = new LinhaModel ();
		linhaModel = linhaService.updateLine(id, linhaDeOnibusModel);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}
	
	@GetMapping("/busca")
	public ResponseEntity<Collection<LinhaModel>> findAll() {
		Collection<LinhaModel> result = new ArrayList<>();
		result = linhaService.findAll();
		if (result == null) {
			return new ResponseEntity<Collection<LinhaModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<LinhaModel>>(result, HttpStatus.OK);
	}
	
	@GetMapping("/busca/{id}")
	public ResponseEntity<LinhaModel> findLineById(@PathVariable("id") int id) {
		LinhaModel linhaModel = new LinhaModel ();
		linhaModel = linhaService.findLineById(id);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}
	
	@DeleteMapping("/exclui/{id}")
	public ResponseEntity<LinhaModel> deleteLine(@PathVariable("id") int id) {
		LinhaModel linhaModel = new LinhaModel ();
		linhaModel = linhaService.deleteLine(id);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}
}
