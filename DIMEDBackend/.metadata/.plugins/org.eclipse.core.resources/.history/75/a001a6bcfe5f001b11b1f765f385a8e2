package controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.backendDIMED.controller.LinhaController;
import com.backendDIMED.model.LinhaModel;
import com.backendDIMED.service.LinhaService;
import com.fasterxml.jackson.databind.ObjectMapper;

import service.ApplicationTestConfig;

import org.springframework.test.context.ContextConfiguration;

@SpringBootTest(classes = LinhaControllerTests.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationTestConfig.class)
@WebAppConfiguration
public class LinhaControllerTests {

	@InjectMocks
	private LinhaController linhaController;

	@MockBean
	private LinhaService linhaService;

	private MockMvc mockMvc;

	List<LinhaModel> linhas = new ArrayList<LinhaModel>();

	LinhaModel linhaModel = new LinhaModel();

	@Before
	public void initLinha() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(linhaController)
				.setControllerAdvice(new ExceptionControllerAdvice()).build();

		List<LinhaModel> linha = new ArrayList<LinhaModel>();

		LinhaModel l1 = new LinhaModel();
		l1.setCodigo("250-11");
		l1.setId(4);
		l1.setNome("1 DE MAIO");

		linha.add(l1);
		linhas = linha;

		linhaModel = l1;

	}

	@Test
	public void testGetLineByNome() throws Exception {
		Mockito.when(linhaService.findLineByNome(linhaModel.getNome())).thenReturn(linhas);
		this.mockMvc.perform(get("/api/linha/busca/nome/1 DE MAIO").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

	@Test
	public void testGetLineById() throws Exception {
		Mockito.when(linhaService.findLineById(4)).thenReturn(linhaModel);
		this.mockMvc.perform(get("/api/linha/busca/4").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json")).andExpect(jsonPath("$.id").value(4))
				.andExpect(jsonPath("$.codigo").value("250-11")).andExpect(jsonPath("$.nome").value("1 DE MAIO"));
	}

	@Test
	public void testGetLine() throws Exception {
		Mockito.when(linhaService.findLine()).thenReturn(linhas);
		this.mockMvc.perform(get("/api/linha").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json")).andExpect(jsonPath("$.[0].id").value(4))
				.andExpect(jsonPath("$.[0].codigo").value("250-11"))
				.andExpect(jsonPath("$.[0].nome").value("1 DE MAIO"));
	}

	@Test
	public void testGetFindAll() throws Exception {
		Mockito.when(linhaService.findAll()).thenReturn(linhas);
		this.mockMvc.perform(get("/api/linha/busca").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json")).andExpect(jsonPath("$.[0].id").value(4))
				.andExpect(jsonPath("$.[0].codigo").value("250-11"))
				.andExpect(jsonPath("$.[0].nome").value("1 DE MAIO"));
	}

	@Test
	public void testSaveLine() throws Exception {
		Mockito.when(linhaService.saveLine(linhaModel.getId(), linhaModel)).thenReturn(linhaModel);
		this.mockMvc.perform(get("/api/linha/busca").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType("application/json")).andExpect(jsonPath("$.[0].id").value(4))
				.andExpect(jsonPath("$.[0].codigo").value("250-11"))
				.andExpect(jsonPath("$.[0].nome").value("1 DE MAIO"));
	}

}
