package com.backendDIMED.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backendDIMED.model.LinhaModel;
import com.backendDIMED.service.LinhaDeOnibusService;
import com.backendDIMED.service.LinhaService;

@RestController
@RequestMapping("api/linha")
public class LinhaController {

	@Autowired
	private LinhaDeOnibusService linhaDeOnibusService;
	@Autowired
	private LinhaService linhaService;
	
	

	@GetMapping
	public ResponseEntity<List<LinhaModel>> getLinhasDeOnibus() {
		ResponseEntity<List<LinhaModel>> result = linhaService.findLine();
		return result;
	}

	// parte 2 
	@GetMapping("nome/{nome}")
	public ResponseEntity<List<LinhaModel>> getLinhasDeOnibusPorNome(@PathVariable("nome") String nome) {
		ResponseEntity<List<LinhaModel>> result = linhaService.findLineByNome(nome);
		return result;
	}
	
	// parte 3 
	
	@PostMapping("/adiciona/{id}")
	public ResponseEntity<LinhaModel> adicionarLinhaDeOnibus(@PathVariable("id") int id, @RequestBody LinhaModel linhaDeOnibusModel) {
		ResponseEntity<LinhaModel> result = linhaService.saveLine(id, linhaDeOnibusModel);
		return result;
	}
	
	@PutMapping("/atualiza/{id}")
	public ResponseEntity<LinhaModel> atualizarLinhaDeOnibus(@PathVariable("id") int id, @RequestBody LinhaModel linhaDeOnibusModel) {
		ResponseEntity<LinhaModel> result = linhaService.updateLine(id, linhaDeOnibusModel);
		return result;
	}
	
	@GetMapping("/todos")
	public ResponseEntity<List<LinhaModel>> getLinhasDeOnibusTodos() {
		ResponseEntity<List<LinhaModel>> result = linhaService.findAll();
		return result;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<LinhaModel> getLinhasDeOnibusPorId(@PathVariable("id") int id) {
		ResponseEntity<LinhaModel> result = linhaService.findLineById(id);
		return result;
	}
	
	@DeleteMapping("/exclui/{id}")
	public ResponseEntity<LinhaModel> deleteLinhaDeOnibus(@PathVariable("id") int id) {
		ResponseEntity<LinhaModel> result = linhaService.deleteLine(id);
		return result;
	}
}
