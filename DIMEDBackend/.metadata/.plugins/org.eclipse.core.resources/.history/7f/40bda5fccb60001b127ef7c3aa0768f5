package com.backendDIMED.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.backendDIMED.model.ItinerarioModel;
import com.backendDIMED.model.LinhaModel;
import com.backendDIMED.repository.LinhaRepository;

@Service
public class LinhaServiceImpl implements LinhaService {
	private final String URI = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";

	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	private LinhaRepository linhaDeOnibusRepository;

	@Autowired
	private ItinerarioService itinerarioService;

	public LinhaServiceImpl() {
		super();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
		restTemplate.getMessageConverters().add(converter);
	}

	@Override
	public List<LinhaModel> findLine() {
		ResponseEntity<List<LinhaModel>> result = restTemplate.exchange(URI, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LinhaModel>>() {
				});
		for (LinhaModel linhaDeOnibus : result.getBody()) {
			linhaDeOnibusRepository.save(linhaDeOnibus);
		}
		return result.getBody();
	}

	@Override
	public List<LinhaModel> findLineByNome(String nome) {
		List<LinhaModel> resultBancoDeDados = linhaDeOnibusRepository.findAll();
		List<LinhaModel> resultFiltroNome = new ArrayList<>();
		nome = nome.toUpperCase();
		for (LinhaModel linhaDeOnibus : resultBancoDeDados) {
			if (linhaDeOnibus.getNome().contains(nome)) {
				resultFiltroNome.add(linhaDeOnibus);
			}
		}
		return resultFiltroNome;
	}

	@Override
	public LinhaModel saveLine(int id, LinhaModel linhaDeOnibusModel) {
		Optional<LinhaModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			if (!result.get().getNome().equals(linhaDeOnibusModel.getNome())) {
				result.get().setNome(linhaDeOnibusModel.getNome());
			}
			if (!result.get().getCodigo().equals(linhaDeOnibusModel.getCodigo())) {
				result.get().setCodigo(linhaDeOnibusModel.getCodigo());
			}
			result.get().setId(id);
			linhaDeOnibusRepository.save(result.get());
			return result.get();
		} else {
			linhaDeOnibusModel.setId(id);
			linhaDeOnibusRepository.save(linhaDeOnibusModel);
			return linhaDeOnibusModel;

		}
	}

	@Override
	public List<LinhaModel> findAll() {
		List<LinhaModel> result = linhaDeOnibusRepository.findAll();
		return result;
	}

	@Override
	public LinhaModel findLineById(int id) {
		Optional<LinhaModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			return result.get();
		}
		return null;
	}

	@Override
	public LinhaModel updateLine(int id, LinhaModel linhaDeOnibusModel) {
		if (!linhaDeOnibusRepository.existsById(id)) {
			return null;
		}
		linhaDeOnibusModel.setId(id);
		linhaDeOnibusRepository.save(linhaDeOnibusModel);
		return linhaDeOnibusModel;
	}

	@Override
	public LinhaModel deleteLine(int id) {
		Optional<LinhaModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			linhaDeOnibusRepository.deleteById(id);
			return result.get();
		}
		return null;
	}

	@Override
	public Collection<LinhaModel> findLineByLightning(double lat, double lng, double raio) throws InterruptedException {
		List<LinhaModel> listLinha = new ArrayList<>();
		List<LinhaModel> resultLatLng = new ArrayList<>();
		List<ItinerarioModel> listItinerario = new ArrayList<>();
		
		// o método findLine busca todas as linhas de onibus na URI e a
		// variavel
		listLinha = findLine();
	

		// aqui eu busco as informações para popular o itinerario
		for(int i=0; i< listLinha.size(); i++) {
			ItinerarioModel itinerarioModel = new ItinerarioModel();
			itinerarioModel = itinerarioService.findById(listLinha.get(i).getId());
			Thread.sleep(1000);
			listItinerario.add(itinerarioModel);
		}

		
		for(ItinerarioModel itinerarioModel : listItinerario ) {
			for(int i=0; i< itinerarioModel.getLagLon().size(); i++) {
				double resultDistancia = 0;
				resultDistancia = calculeDistancia(lat, itinerarioModel.getLagLon().get(i).getLat(), lng, itinerarioModel.getLagLon().get(i).getLng(), 0.0, 0.0);
				Thread.sleep(100);
				if (resultDistancia <= raio) {
					resultLatLng.add(listLinha.get(i));
					break;
				}
			}
		}
		return resultLatLng;
	}

	public static double calculeDistancia(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {

		// raio da terra
		final int RAIO_TERRA = 6371;

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double result = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double result2 = 2 * Math.atan2(Math.sqrt(result), Math.sqrt(1 - result));
		double distance = RAIO_TERRA * result2 * 1000;

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}

}
