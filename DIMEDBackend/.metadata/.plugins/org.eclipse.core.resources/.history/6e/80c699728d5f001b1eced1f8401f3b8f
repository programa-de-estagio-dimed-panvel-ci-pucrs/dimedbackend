package com.backendDIMED.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.backendDIMED.model.LinhaModel;
import com.backendDIMED.repository.LinhaRepository;

@Service
public class LinhaServiceImpl implements LinhaService {
	private final String URI = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";

	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	private LinhaRepository linhaDeOnibusRepository;

	public LinhaServiceImpl() {
		super();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
		restTemplate.getMessageConverters().add(converter);
	}

	@Override
	public ResponseEntity<List<LinhaModel>> findLine() {
		ResponseEntity<List<LinhaModel>> result = restTemplate.exchange(URI, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LinhaModel>>() {
				});
		for (LinhaModel linhaDeOnibus : result.getBody()) {
			linhaDeOnibusRepository.save(linhaDeOnibus);
		}
		return ResponseEntity.ok(result.getBody());
	}

	@Override
	public ResponseEntity<List<LinhaModel>> findLineByNome(@PathVariable("nome") String nome) {
		List<LinhaModel> resultBancoDeDados = linhaDeOnibusRepository.findAll();
		List<LinhaModel> resultFiltroNome = new ArrayList<>();
		nome = nome.toUpperCase();
		for (LinhaModel linhaDeOnibus : resultBancoDeDados) {
			if (linhaDeOnibus.getNome().contains(nome)) {
				resultFiltroNome.add(linhaDeOnibus);
			}
		}
		return ResponseEntity.ok(resultFiltroNome);
	}

	@Override
	public ResponseEntity<LinhaModel> saveLine(@PathVariable("id") int id, @RequestBody LinhaModel linhaDeOnibusModel) {
		Optional<LinhaModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			if (!result.get().getNome().equals(linhaDeOnibusModel.getNome())) {
				result.get().setNome(linhaDeOnibusModel.getNome());
			}
			if (!result.get().getCodigo().equals(linhaDeOnibusModel.getCodigo())) {
				result.get().setCodigo(linhaDeOnibusModel.getCodigo());
			}
			result.get().setId(id);
			linhaDeOnibusRepository.save(result.get());
			return ResponseEntity.ok(result.get());
		} else {
			linhaDeOnibusModel.setId(id);
			linhaDeOnibusRepository.save(linhaDeOnibusModel);
			return ResponseEntity.ok(linhaDeOnibusModel);

		}
	}

	@Override
	public ResponseEntity<List<LinhaModel>> findAll() {
		List<LinhaModel> result = linhaDeOnibusRepository.findAll();
		return ResponseEntity.ok(result);
	}

	@Override
	public ResponseEntity<LinhaModel> findLineById(@PathVariable("id") int id) {
		Optional<LinhaModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			return ResponseEntity.ok(result.get());
		}
		return ResponseEntity.notFound().build();
	}

	@Override
	public ResponseEntity<LinhaModel> updateLine(@PathVariable("id") int id, @RequestBody LinhaModel linhaDeOnibusModel) {
		if (!linhaDeOnibusRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		linhaDeOnibusModel.setId(id);
		linhaDeOnibusRepository.save(linhaDeOnibusModel);
		return ResponseEntity.ok(linhaDeOnibusModel);
	}

	@Override
	public ResponseEntity<LinhaModel> deleteLine(@PathVariable("id") int id) {
		Optional<LinhaModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			linhaDeOnibusRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
}
