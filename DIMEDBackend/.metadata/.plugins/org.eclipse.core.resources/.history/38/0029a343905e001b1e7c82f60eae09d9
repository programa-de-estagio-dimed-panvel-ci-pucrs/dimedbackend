package com.backendDIMED.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import com.backendDIMED.model.LinhaDeOnibusModel;
import com.backendDIMED.repository.LinhaDeOnibusRepository;

@Service
public class LinhaDeOnibusService {

	private final String URI = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";

	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	private LinhaDeOnibusRepository linhaDeOnibusRepository;

	public LinhaDeOnibusService() {
		super();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
		restTemplate.getMessageConverters().add(converter);
	}

	public ResponseEntity<List<LinhaDeOnibusModel>> getLinhasDeOnibus() {
		ResponseEntity<List<LinhaDeOnibusModel>> result = restTemplate.exchange(URI, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LinhaDeOnibusModel>>() {
				});
		for (LinhaDeOnibusModel linhaDeOnibus : result.getBody()) {
			linhaDeOnibusRepository.save(linhaDeOnibus);
		}
		return ResponseEntity.ok(result.getBody());
	}

	// parte 2 -- criar API para buscar por nomes
	public ResponseEntity<List<LinhaDeOnibusModel>> getLinhasDeOnibusPorNome(@PathVariable("nome") String nome) {
		List<LinhaDeOnibusModel> resultBancoDeDados = linhaDeOnibusRepository.findAll();
		List<LinhaDeOnibusModel> resultFiltroNome = new ArrayList<>();
		nome = nome.toUpperCase();
		for (LinhaDeOnibusModel linhaDeOnibus : resultBancoDeDados) {
			if (linhaDeOnibus.getNome().contains(nome)) {
				resultFiltroNome.add(linhaDeOnibus);
			}
		}
		return ResponseEntity.ok(resultFiltroNome);
	}

	// parte 3 -- CRUD
	public ResponseEntity<LinhaDeOnibusModel> adicionarLinhaDeOnibus(@PathVariable("id") int id,
			@RequestBody LinhaDeOnibusModel linhaDeOnibusModel) {
		int ultimoId = 0;
		List<LinhaDeOnibusModel> resultBancoDeDados = linhaDeOnibusRepository.findAll();
		for (LinhaDeOnibusModel linhaDeOnibus : resultBancoDeDados) {
			ultimoId = linhaDeOnibus.getId();
		}
		if (linhaDeOnibusRepository.existsById(id)) {
			if (!result.get().getNome().equals(linhaDeOnibusModel.getNome())) {
				result.get().setNome(linhaDeOnibusModel.getNome());
			}
			if (!result.get().getCodigo().equals(linhaDeOnibusModel.getCodigo())) {
				result.get().setCodigo(linhaDeOnibusModel.getCodigo());
			}
			result.get().setId(id);
			linhaDeOnibusRepository.save(result.get());
			return ResponseEntity.ok(result.get());
		}

		linhaDeOnibusModel.setId(++ultimoId);
		linhaDeOnibusRepository.save(linhaDeOnibusModel);
		return ResponseEntity.ok(result.get());

	}

	public ResponseEntity<List<LinhaDeOnibusModel>> getLinhasDeOnibusTodos() {
		List<LinhaDeOnibusModel> result = linhaDeOnibusRepository.findAll();
		return ResponseEntity.ok(result);
	}

	public ResponseEntity<LinhaDeOnibusModel> getLinhasDeOnibusPorId(@PathVariable("id") int id) {
		Optional<LinhaDeOnibusModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			return ResponseEntity.ok(result.get());
		}
		return ResponseEntity.notFound().build();
	}

	public ResponseEntity<LinhaDeOnibusModel> deleteLinhaDeOnibus(@PathVariable("id") int id) {
		Optional<LinhaDeOnibusModel> result = linhaDeOnibusRepository.findById(id);
		if (result.isPresent()) {
			linhaDeOnibusRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();

	}

}
