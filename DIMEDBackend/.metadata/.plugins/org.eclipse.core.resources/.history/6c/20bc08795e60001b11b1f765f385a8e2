package com.backendDIMED.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backendDIMED.model.LinhaModel;

import com.backendDIMED.service.LinhaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Linha de Ônibus", tags = { "Linhas de Ônibus API" })
@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/linha")
public class LinhaController {

	@Autowired
	private LinhaService linhaService;

	@GetMapping
	@ApiOperation(value = "Esse método lista todas as linhas de ônibus", notes = "Esse método está consumindo a API externa, ao trazer o resultado dessa consulta o metodo persiste ela no banco de dados h2. ")
	public ResponseEntity<Collection<LinhaModel>> findLine() {
		Collection<LinhaModel> result = new ArrayList<>();
		result = linhaService.findLine();
		if (result == null) {
			return new ResponseEntity<Collection<LinhaModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<LinhaModel>>(result, HttpStatus.OK);
	}

	// parte 2
	@ApiOperation(value = "Esse método lista as linhas de ônibus filtradas pelo nome", notes="Esse método está consumindo o banco de dados H2.")
	@GetMapping("/busca/nome/{nome}")
	public ResponseEntity<Collection<LinhaModel>> findLineByNome(@PathVariable("nome") String nome) {
		Collection<LinhaModel> result = new ArrayList<>();
		result = linhaService.findLineByNome(nome);
		if (result == null) {
			return new ResponseEntity<Collection<LinhaModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<LinhaModel>>(result, HttpStatus.OK);
	}

	// parte 3
	@ApiOperation(value = "Esse método adiciona uma nova linha de ônibus.", notes=" Esse método está consumindo o banco de dados H2.")
	@PostMapping("/adiciona/{id}")
	public ResponseEntity<LinhaModel> saveLine(@PathVariable("id") int id,
			@RequestBody LinhaModel linhaDeOnibusModelAdiciona) {
		LinhaModel linhaModel = new LinhaModel();
		linhaModel = linhaService.saveLine(id, linhaDeOnibusModelAdiciona);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}

	@PutMapping("/atualiza/{id}")
	@ApiOperation(value = "Esse método atualiza uma linha de ônibus existente, caso a linha passada não exista, o metodo retorna status HTTP NOT_FOUND.", notes=" Esse método está consumindo o banco de dados H2.")
	public ResponseEntity<LinhaModel> updateLine(@PathVariable("id") int id,
			@RequestBody LinhaModel linhaDeOnibusModel) {
		LinhaModel linhaModel = new LinhaModel();
		linhaModel = linhaService.updateLine(id, linhaDeOnibusModel);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}

	@ApiOperation(value = "Esse método lista todas as linhas de ônibus.", notes=" Esse método está consumindo o banco de dados H2.")
	@GetMapping("/busca")
	public ResponseEntity<Collection<LinhaModel>> findAll() {
		Collection<LinhaModel> result = new ArrayList<>();
		result = linhaService.findAll();
		if (result == null) {
			return new ResponseEntity<Collection<LinhaModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<LinhaModel>>(result, HttpStatus.OK);
	}

	@ApiOperation(value = "Esse método lista uma linha de ônibus filtrada pelo id, caso o id passado não exista, o método retorna status HTTP NOT_FOUND.", notes=" Esse método está consumindo o banco de dados H2.")
	@GetMapping("/busca/{id}")
	public ResponseEntity<LinhaModel> findLineById(@PathVariable("id") int id) {
		LinhaModel linhaModel = new LinhaModel();
		linhaModel = linhaService.findLineById(id);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}

	@DeleteMapping("/exclui/{id}")
	@ApiOperation(value = "Esse método exclui uma linha de ônibus filtrada pelo o id, caso o id passado não exista, o método retorna status HTTP NOT_FOUND.", notes=" Esse método está consumindo o banco de dados H2.")
	public ResponseEntity<LinhaModel> deleteLine(@PathVariable("id") int id) {
		LinhaModel linhaModel = new LinhaModel();
		linhaModel = linhaService.deleteLine(id);
		if (linhaModel == null) {
			return new ResponseEntity<LinhaModel>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<LinhaModel>(linhaModel, HttpStatus.OK);
	}

	// parte 4

	@GetMapping("/busca/lightning")
	@ApiOperation(value = "Esse método lista as linhas de onibus filtradas pelas coordenadas.", notes=" Esse método está consumindo o banco de dados H2.")
	public ResponseEntity<Collection<LinhaModel>> findLineByLightning() throws InterruptedException {
		Collection<LinhaModel> linhaModel = new ArrayList<>();
		double lat = -30.14598885554;
		double lng = -51.41241544218;
		double raio = 2;
		linhaModel = linhaService.findLineByLightning(lat, lng, raio);
		if (linhaModel == null) {
			return new ResponseEntity<Collection<LinhaModel>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Collection<LinhaModel>>(linhaModel, HttpStatus.OK);
	}
}
